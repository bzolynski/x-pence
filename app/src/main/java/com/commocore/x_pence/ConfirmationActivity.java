package com.commocore.x_pence;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ConfirmationActivity extends EntryBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        super.retrieveEntryData();

        TextView expenseValueText = (TextView) findViewById(R.id.expenseValueSummary);
        expenseValueText.setText(entryData.getExpense());

        TextView categoryValueText = (TextView) findViewById(R.id.categoryValueSummary);
        int categoryValue = entryData.getCategory();
        categoryValueText.setText(Integer.toString(categoryValue));

        TextView dateValueText = (TextView) findViewById(R.id.dateValueSummary);
        dateValueText.setText(entryData.getDate());
    }

    public void submitStep(View view) {

    }

    @Override
    protected void setBackActivity() {
        Intent intent = new Intent(this, DateActivity.class);
        putExtrasToIntent(intent);
        startActivity(intent);
    }
}
