package com.commocore.x_pence;

/**
 * Entry data class used for intents
 */
public class EntryData {
    String expense;
    int category;
    String date;

    public EntryData() {
        setExpense(null);
        setCategory(0);
        setDate(null);
    }

    public String getExpense() {
        return expense;
    }

    public int getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }

    public void setExpense(String value) {
        if (value == null) {
            expense = "0";
        } else {
            Float floatValue = Float.valueOf(value);
            expense = String.valueOf(floatValue);
        }
    }

    public void setCategory(int value) {
        category = value;
    }

    public void setDate(String value) {
        date = value;
    }

    public boolean isExpense() {
        return getExpense() != null;
    }

    public boolean isCategory() {
        return getCategory() != 0;
    }

    public boolean isDate() {
        return getDate() != null;
    }
}
