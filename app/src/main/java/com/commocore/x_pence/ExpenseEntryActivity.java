package com.commocore.x_pence;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ExpenseEntryActivity extends EntryBaseActivity {

    TextView expenseValueText;
    Button clearExpenseValueButton;
    Button enterExpenseValueButton;
    Boolean pointFlag = false;
    int pointPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_entry);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        expenseValueText = (TextView) findViewById(R.id.expenseValueText);
        expenseValueText.setText("0");

        clearExpenseValueButton = (Button) findViewById(R.id.clearExpenseValueButton);
        clearExpenseValueButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                expenseValueText.setText("0");
                pointFlag = false;
                pointPosition = 0;
                hideEnterExpenseValueButton();
            }
        });

        enterExpenseValueButton = (Button) findViewById(R.id.enterExpenseValueButton);

        Button buttonDeleteLastCharacter = (Button) findViewById(R.id.delete_last_character);
        buttonDeleteLastCharacter.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteLastCharacter();
            }
        });

        super.retrieveEntryData();

        String expenseValue = parseExpenseValue(entryData.getExpense());
        expenseValueText = (TextView) findViewById(R.id.expenseValueText);
        expenseValueText.setText(expenseValue);

        preparePointFlag(expenseValue);

        if (!isExpenseValue(expenseValue)) {
            hideEnterExpenseValueButton();
        }

        setDebugEntryData(); // TODO: Remove
    }

    @NonNull
    private String parseExpenseValue(String value) {
        String expenseValue;

        if (value == null) {
            expenseValue = "0";
        } else {
            expenseValue = value;
        }
        return expenseValue;
    }

    private void preparePointFlag(String value) {
        if (value.contains(".")) {
            pointFlag = true;
            String[] values = value.split("\\.");
            pointPosition = values[1].length() + 1;
        }
    }

    public void addCharacter(View view) {
        if (pointFlag) {
            if (pointPosition >= 3) {
                return;
            }
            incrementPointPosition();
        }
        String character = getButtonString(view);
        String value = expenseValueText.getText().toString();
        if (getExpenseFloatValue(value) == 0 && !pointFlag) {
            value = "";
        }

        // Check maximum limit for integer part
        if (!pointFlag && value.length() >= 9) {
            return;
        }

        String newValue = value + character;

        expenseValueText.setText(newValue);

        if (isExpenseValue(newValue)) {
            showEnterExpenseValueButton();
        }
    }

    public void addPoint(View view) {
        String value = expenseValueText.getText().toString();
        if (!pointFlag) {
            if (value.length() == 0) {
                value = "0";
            }
            expenseValueText.setText(value + ".");
            pointFlag = true;
            incrementPointPosition();
        }
    }

    private void incrementPointPosition() {
        pointPosition += 1;
    }

    private void decrementPointPosition() {
        if (pointFlag) {
            pointPosition -= 1;
            if (pointPosition == 0) {
                pointFlag = false;
            }
        }
    }

    private String getButtonString(View view) {
        Button button = (Button) view;
        return button.getText().toString();
    }

    public void nextStep(View view) {

        String value = expenseValueText.getText().toString();

        // Return if no value
        if (value.contentEquals("")) {
            return;
        }
        entryData.setExpense(value);

        Intent intent = new Intent(this, CategoryActivity.class);
        putExtrasToIntent(intent);

        startActivity(intent);
    }

    @Override
    protected void setBackActivity() {

    }

    private void deleteLastCharacter() {
        String value = expenseValueText.getText().toString();
        int length = value.length();
        if (length == 0) {
            return;
        }

        decrementPointPosition();
        length -= 1;

        String newValue = value.substring(0, length);
        if (newValue.length() == 0) {
            newValue = "0";
        }
        expenseValueText.setText(newValue);

        if (!isExpenseValue(newValue)) {
            hideEnterExpenseValueButton();
        }
    }

    private float getExpenseFloatValue(String value) {
        if (value.equals("")) {
            return 0;
        }
        return Float.valueOf(value);
    }

    private boolean isExpenseValue(String value) {
        return getExpenseFloatValue(value) > 0;
    }

    private void showEnterExpenseValueButton() {
        enterExpenseValueButton.setVisibility(View.VISIBLE);
    }

    private void hideEnterExpenseValueButton() {
        enterExpenseValueButton.setVisibility(View.INVISIBLE);
    }
}
