package com.commocore.x_pence;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class DateActivity extends EntryBaseActivity {

    DatePicker dateValue;
    TextView dateValueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dateValue = (DatePicker) findViewById(R.id.dateValue);
        dateValueText = (TextView) findViewById(R.id.dateValueText);

        dateValue.getCalendarView().setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                DateValueObject dateVO = new DateValueObject(year, month, dayOfMonth);
                dateValueText.setText(dateVO.getStringValue());
            }
        });

        super.retrieveEntryData();

        if (!entryData.isDate() || entryData.getDate().equals("")) {
            setFieldsWithCurrentDate();
        } else {
            DateValueObject dateVO = new DateValueObject(entryData.getDate());
            dateValue.updateDate(dateVO.getYear(), dateVO.getMonth(), dateVO.getDay());
            dateValueText.setText(dateVO.getStringValue());
        }
        setDebugEntryData(); // TODO: Remove
    }

    public void nextStep(View view) {
        setEntryDataFromField();

        Intent intent = new Intent(this, ConfirmationActivity.class);
        putExtrasToIntent(intent);

        startActivity(intent);
    }

    private void setFieldsWithCurrentDate() {
        Calendar now = Calendar.getInstance();
        dateValue.updateDate(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        DateValueObject dateVO = new DateValueObject(
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dateValueText.setText(dateVO.getStringValue());
    }

    @Override
    protected void setBackActivity() {
        setEntryDataFromField();

        Intent intent = new Intent(this, CategoryActivity.class);
        putExtrasToIntent(intent);
        startActivity(intent);
    }

    private void setEntryDataFromField() {
        DateValueObject dateVO = new DateValueObject(
                dateValue.getYear(),
                dateValue.getMonth(),
                dateValue.getDayOfMonth()
        );
        entryData.setDate(dateVO.getStringValue());
    }
}
