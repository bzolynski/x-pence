package com.commocore.x_pence;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

abstract public class EntryBaseActivity extends BaseActivity {

    public final static String EXPENSE_VALUE = "com.commocore.x_pence.EXPENSE_VALUE";
    public final static String CATEGORY_VALUE = "com.commocore.x_pence.CATEGORY_VALUE";
    public final static String DATE_VALUE = "com.commocore.x_pence.DATE_VALUE";

    EntryData entryData;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void retrieveEntryData() {
        entryData = new EntryData();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            entryData.setExpense(extras.getString(EntryBaseActivity.EXPENSE_VALUE));
            entryData.setCategory(extras.getInt(EntryBaseActivity.CATEGORY_VALUE));
            entryData.setDate(extras.getString(EntryBaseActivity.DATE_VALUE));
        }
    }

    /**
     * When main Back button pressed
     */
    @Override
    public void onBackPressed() {
        setBackActivity();
    }

    public void putExtrasToIntent(Intent intent) {
        intent.putExtra(EXPENSE_VALUE, entryData.getExpense());
        intent.putExtra(CATEGORY_VALUE, entryData.getCategory());
        intent.putExtra(DATE_VALUE, entryData.getDate());
    }

    abstract protected void setBackActivity();

    // TODO: Remove
    public void setDebugEntryData() {
        TextView expenseValueText = (TextView) findViewById(R.id.expenseValueSummary);
        expenseValueText.setText(entryData.getExpense());

        TextView categoryValueText = (TextView) findViewById(R.id.categoryValueSummary);
        int categoryValue = entryData.getCategory();
        categoryValueText.setText(Integer.toString(categoryValue));

        TextView dateValueText = (TextView) findViewById(R.id.dateValueSummary);
        dateValueText.setText(entryData.getDate());
    }
}
