package com.commocore.x_pence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryEntry {
    protected int id;
    protected String name;
    protected int colour;

    public CategoryEntry(int id, String name, int colour) {
        this.id = id;
        this.name = name;
        this.colour = colour;
    }

    // Constructor to convert JSON object into a Java class instance
    public CategoryEntry(JSONObject object){
        try {
            this.id = object.getInt("id");
            this.name = object.getString("name");
            this.colour = object.getInt("colour");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Factory method to convert an array of JSON objects into a list of objects
    // CategoryEntry.fromJson(jsonArray);
    public static ArrayList<CategoryEntry> fromJson(JSONArray jsonObjects) {
        ArrayList<CategoryEntry> users = new ArrayList<CategoryEntry>();
        for (int i = 0; i < jsonObjects.length(); i++) {
            try {
                users.add(new CategoryEntry(jsonObjects.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return users;
    }

    @Override
    public String toString() {
        return name;
    }
}
