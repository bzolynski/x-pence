package com.commocore.x_pence;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateValueObject {

    final static String DATE_FORMAT = "yyyy-MM-dd";

    private Date date;
    private Calendar calendar;

    public DateValueObject(String value) {
        DateFormat dateFormat = getDateFormat();
        if (!parse(dateFormat, value)) {
            date = new Date();
        }
        setCalendarFromDate();
    }

    public DateValueObject(int year, int month, int dayOfMonth) {
        calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        date = calendar.getTime();
    }

    public int getYear() {
        return calendar.get(Calendar.YEAR);
    }

    public int getMonth() {
        return calendar.get(Calendar.MONTH);
    }

    public int getDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public String getStringValue() {
        DateFormat dateFormat = getDateFormat();
        return dateFormat.format(date);
    }

    @NonNull
    private DateFormat getDateFormat() {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setLenient(false);
        return dateFormat;
    }

    private boolean parse(DateFormat dateFormat, String value) {
        try {
            date = dateFormat.parse(value);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private void setCalendarFromDate() {
        calendar = Calendar.getInstance();
        calendar.setTime(date);
    }
}
