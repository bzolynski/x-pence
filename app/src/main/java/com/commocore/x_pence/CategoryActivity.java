package com.commocore.x_pence;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

public class CategoryActivity extends EntryBaseActivity {

    TextView categoryValueText;
    ArrayList<CategoryEntry> categories;
    Button enterCategoryValueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        categoryValueText = (TextView) findViewById(R.id.categoryValueText);
        enterCategoryValueButton = (Button) findViewById(R.id.enterCategoryValueButton);

        super.retrieveEntryData();

        createFakeCategoriesData(); // TODO: Setting categories, hardcoded values here...

        CategoryEntryAdapter adapter = new CategoryEntryAdapter(this, categories);

        GridView categoriesListView = (GridView) findViewById(R.id.categoriesList);
        categoriesListView.setAdapter(adapter);

        setCategoryFieldFromDataEntry();

        setDebugEntryData(); // TODO: Remove
    }

    private void createFakeCategoriesData() {
        categories = new ArrayList<>();
        addCategory(1, "Food", 0xff40ff00);
        addCategory(2, "Drink", 0xff00ffbf);
        addCategory(3, "Going out", 0xffff0040);
        addCategory(4, "Clothes", 0xffff8000);
        addCategory(5, "Bills", 0xffffff20);
        addCategory(6, "Others", 0xff66ffa0);
        addCategory(7, "Vinyl records", 0xffeeff33);
        addCategory(8, "Computer games", 0xffaaff88);
        addCategory(9, "Books", 0xff40ff00);
        addCategory(10, "Charity", 0xff40ff00);
        addCategory(11, "Betting", 0xff4055ea);
    }

    private void addCategory(int id, String name, int colour) {
        CategoryEntry categoryEntry = new CategoryEntry(id, name, colour);
        categories.add(categoryEntry);
    }

    private void setCategoryFieldFromDataEntry() {
        if (entryData.isCategory()) {
            for (CategoryEntry category : categories ) {
                if (category.id == entryData.getCategory()) {
                    categoryValueText.setText(category.name);
                    return;
                }
            }
        }
        hideEnterExpenseValueButton();
        categoryValueText.setText("?");
    }

    public void nextStep(View view) {
        Intent intent = new Intent(this, DateActivity.class);
        putExtrasToIntent(intent);

        startActivity(intent);
    }

    public void selectCategory(View view) {
        CategoryButton button = (CategoryButton) view;
        String categoryName = button.getText().toString();
        categoryValueText.setText(categoryName);
        setEntryDataFromField(button);
        showEnterCategoryValueButton();
    }

    private void setEntryDataFromField(CategoryButton button) {
        int value = button.getCategoryId();
        entryData.setCategory(value);
    }

    //@Override
    protected void setBackActivity() {
        Intent intent = new Intent(this, ExpenseEntryActivity.class);
        putExtrasToIntent(intent);
        startActivity(intent);
    }

    private void showEnterCategoryValueButton() {
        enterCategoryValueButton.setVisibility(View.VISIBLE);
    }

    private void hideEnterExpenseValueButton() {
        enterCategoryValueButton.setVisibility(View.INVISIBLE);
    }
}
