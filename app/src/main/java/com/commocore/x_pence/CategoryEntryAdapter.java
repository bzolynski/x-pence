package com.commocore.x_pence;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Adapter for converting ArrayList to view
 * Uses ViewHolder Pattern
 */
class CategoryEntryAdapter extends ArrayAdapter<CategoryEntry> {

    // View lookup cache
    private static class ViewHolder {
        CategoryButton category;
    }

    public CategoryEntryAdapter(
            Context context,
            ArrayList<CategoryEntry> categories) {
        super(context, R.layout.layout_category_entry, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        CategoryEntry categoryEntry = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.layout_category_entry, parent, false);
            viewHolder.category = (CategoryButton) convertView.findViewById(R.id.category);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Lookup view for data population
        Button category = (Button) convertView.findViewById(R.id.category);

        // Populate the data into the template view using the data object
        viewHolder.category.setText(categoryEntry.name);
        viewHolder.category.setBackgroundColor(categoryEntry.colour);
        viewHolder.category.setCategoryId(categoryEntry.id);

        // Return the completed view to render on screen
        return convertView;
    }
}
